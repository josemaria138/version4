package com.example.version4.model;

import com.example.version4.dao.GifDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Gif implements GifDao {

    private String name;

    private static List<Gif> allGifs = new ArrayList<>();

    static {
        allGifs.add(new Gif("android-explosion.gif"));
        allGifs.add(new Gif("ben-and-mike.gif"));
        allGifs.add(new Gif("book-dominos.gif"));
        allGifs.add(new Gif("compiler-bot.gif"));
        allGifs.add(new Gif("cowboy-coder.gif"));
        allGifs.add(new Gif("infinite-andrew.gif"));

    }

    public Gif(String name) {
        this.name = name;
    }

    public List<String> getUrl(List<Gif> allGifs) {
        List<String> urlList = new ArrayList<>();

        for (Gif gif : Gif.getAllGifs()) {
            String url = gif.getGifName(gif.getName());
            urlList.add(url);
        }

        return urlList;
    }

    @Override
    public String getGifName(String name) {
        name = this.getName();

        return "resources/static/gifs/" + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Gif> getAllGifs() {
        return allGifs;
    }

    public Gif() {
    }

}