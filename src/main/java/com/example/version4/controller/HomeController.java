package com.example.version4.controller;


import com.example.version4.model.Gif;
import com.example.version4.dao.GifDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;


@Controller
public class HomeController {


    @Autowired
    private GifDao gifDao;

    @GetMapping("/")
    public String show(ModelMap map){

    map.put("gifsUrl",gifDao.getUrl(Gif.getAllGifs()));

        return "home";
    }

}