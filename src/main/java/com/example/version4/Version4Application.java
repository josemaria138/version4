package com.example.version4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Version4Application {

    public static void main(String[] args) {
        SpringApplication.run(Version4Application.class, args);
    }
}
