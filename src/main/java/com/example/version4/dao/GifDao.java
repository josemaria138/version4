package com.example.version4.dao;

import com.example.version4.model.Gif;

import java.util.ArrayList;
import java.util.List;

public interface GifDao {

    public String getGifName(String name);

    public List <String> getUrl(List <Gif> list);

}